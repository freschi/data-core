package at.femoweb.data.core;

/**
 * Created by felix on 2/25/15.
 */
public interface Condition {

    public abstract String getTable();
    public abstract String getField();
    public abstract String getValue();
}
