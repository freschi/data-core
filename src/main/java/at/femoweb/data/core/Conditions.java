package at.femoweb.data.core;


/**
 * Created by felix on 2/25/15.
 */
public interface Conditions {

    public abstract Conditions field(String field, String value);
    public abstract Conditions like(String field, String value);
    public abstract Conditions field(Condition condition);
    public abstract Conditions like(Condition condition);

    public abstract Conditions not();
    public abstract Conditions and(Conditions conditions);
    public abstract Conditions or(Conditions conditions);
}
