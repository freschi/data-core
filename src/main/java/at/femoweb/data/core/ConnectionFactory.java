package at.femoweb.data.core;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by felix on 3/2/15.
 */
public interface ConnectionFactory {

    public void openConnection(String url, String user, String password);
    public boolean isSql();
    public boolean isJDBC();
    public Connection getConnection() throws SQLException;
    public Connection getConnection(boolean newConnection) throws SQLException;
    public void closeAll();
    public void destroy();
    public DatabaseDialect getDatabaseDialect();
}
