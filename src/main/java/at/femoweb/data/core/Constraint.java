package at.femoweb.data.core;

import java.util.List;

/**
 * Created by felix on 1/26/15.
 */
public interface Constraint<T> {

    public abstract boolean validOperationInsert(List<T> elements, T insert);
    public abstract boolean validOperationUpdate(List<T> elements, T update);
    public abstract boolean validOperationDelete(List<T> elements, T delete);
}
