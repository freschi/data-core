package at.femoweb.data.core;

import at.femoweb.data.core.annotations.*;
import at.femoweb.data.core.annotations.Table;
import at.femoweb.data.core.internal.factories.MysqlFactory;
import at.femoweb.data.core.internal.query.BasicSchema;
import at.femoweb.data.core.internal.query.BasicTable;

/**
 * Created by felix on 1/26/15.
 */
public final class DataContext {

    public static Schema generateSchema(Class<?> schemaDefinition, ConnectionFactory factory) {
        if(schemaDefinition.isAnnotationPresent(at.femoweb.data.core.annotations.Schema.class) && schemaDefinition.isAnnotationPresent(Tables.class)) {
            BasicSchema schema = new BasicSchema(schemaDefinition.getAnnotation(at.femoweb.data.core.annotations.Schema.class).name(), factory);
            at.femoweb.data.core.annotations.Schema def = schemaDefinition.getAnnotation(at.femoweb.data.core.annotations.Schema.class);
            Tables tables = schemaDefinition.getAnnotation(Tables.class);
            Table[] tableDefs = tables.value();
            for(Table table : tableDefs) {
                Class<?> tableDef = table.value();
                if(!tableDef.isAnnotationPresent(Entity.class)) {
                    throw new UnsupportedOperationException("Table " + tableDef.getName() + " was not correctly annotated");
                }
                Entity entity = tableDef.getAnnotation(Entity.class);
                BasicTable basicTable = new BasicTable(entity.tableName());
                schema.addTable(basicTable);
            }
            return schema;
        } else if (schemaDefinition.isAnnotationPresent(Entity.class)) {
            return generateSchema(schemaDefinition.getAnnotation(Entity.class).schema(), factory);
        } else {
            throw new UnsupportedOperationException("Definition was not correctly annotated");
        }
    }

    public static ConnectionFactory connect(String url, String user, String password) {
        if(url.startsWith("jdbc:mysql:")) {
            MysqlFactory factory = new MysqlFactory();
            factory.openConnection(url, user, password);
            return factory;
        } else {
            throw new UnsupportedOperationException("No factory for url " + url + " found!");
        }
    }

}
