package at.femoweb.data.core;

import java.util.List;

/**
 * Created by felix on 2/25/15.
 */
public interface Query<T> {

    public enum Order {
        ASCENDING, DESCENDING
    }

    public abstract Query<T> select();
    public abstract Query<T> delete();
    public abstract Query<T> update();
    public abstract Query<T> insert();
    public abstract SpecializedQuery field(Class<?> entity, String field);

    public abstract Query<T> where(Class<?> entity, String field, Object value);
    public abstract Query<T> where(Conditions conditions);
    public abstract Query<T> groupBy(String field);
    public abstract Query<T> orderBy(String field, Order order);
    public abstract Query<T> orderBy(String field);
    public abstract Query<T> having(String field, Object value);
    public abstract Query<T> having(Conditions conditions);
    public abstract Query<T> set(String field, Object value);
    public abstract Query<T> set(String field, Object[] values);
    public abstract Query<T> values(T[] objects);

    public abstract List<T> execute();
    public abstract Class<T> type();
}
