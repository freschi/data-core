package at.femoweb.data.core;

import java.io.PrintStream;

/**
 * Created by felix on 1/26/15.
 */
public interface Schema {

    public abstract int getTableCount();
    public abstract Table getTable(int index);
    public abstract Table getTable(String name);
    public abstract Table getTable(Class<?> identifier);
    public abstract void printInfo(PrintStream out);
    public abstract <T> Query<T> query(Class<T> type);
}
