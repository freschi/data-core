package at.femoweb.data.core;

import at.femoweb.data.core.internal.query.DataObject;

/**
 * Created by felix on 3/2/15.
 */
public interface SpecializedQuery extends Query<DataObject> {

    public abstract SpecializedQuery on(Class<?> srcTable, String srcField, Class<?> dstTable, String dstField);
    public abstract SpecializedQuery param(String field);
}
