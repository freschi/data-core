package at.femoweb.data.core;

/**
 * Created by felix on 1/26/15.
 */
public interface ValueConverter<T, F> {

    public abstract F fromDb(T value);
    public abstract T toDb(F value);
}
