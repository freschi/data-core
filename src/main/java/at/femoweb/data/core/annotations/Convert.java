package at.femoweb.data.core.annotations;

import at.femoweb.data.core.ValueConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 1/26/15.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Convert {

    public Class<? extends ValueConverter> converter();
    public Class<?> databaseClass() default String.class;
}
