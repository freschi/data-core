package at.femoweb.data.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 1/26/15.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Entity {

    public enum CreationPolicy {
        DROP_AND_CREATE, CREATE, UPDATE
    }

    public String tableName();
    public Class<?> schema();
    public CreationPolicy creationPolicy();
}
