package at.femoweb.data.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 1/26/15.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Field {

    /*public enum DataType {
        AUTO, SERIAL, STRING, INTEGER, DECIMAL, DATE
    }*/

    public String name();
    public String table() default "";
    //public DataType type() default DataType.AUTO;
}
