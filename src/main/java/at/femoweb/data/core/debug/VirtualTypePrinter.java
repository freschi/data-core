package at.femoweb.data.core.debug;

import at.femoweb.data.core.annotations.Param;
import at.femoweb.data.core.annotations.VirtualType;
import at.femoweb.data.core.internal.query.DataObject;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by felix on 3/2/15.
 */
public class VirtualTypePrinter {

    public static void printVirtualType(Class<? extends DataObject> type) {
        printVirtualType(System.out, type);
    }

    public static void printVirtualType(PrintStream printStream, Class<? extends DataObject> type) {
        printStream.println("Analyzing " + type.getName());
        if(!type.isAnnotationPresent(VirtualType.class)) {
            printStream.println("No VirtualType!");
            return;
        }
        ArrayList<String> fields = new ArrayList<>();
        ArrayList<String> params = new ArrayList<>();
        for(Field field : type.getDeclaredFields()) {
            if(field.isAnnotationPresent(at.femoweb.data.core.annotations.Field.class)) {
                at.femoweb.data.core.annotations.Field f = field.getAnnotation(at.femoweb.data.core.annotations.Field.class);
                fields.add(f.table() + "." + f.name());
            } else if (field.isAnnotationPresent(Param.class)) {
                Param param = field.getAnnotation(Param.class);
                params.add(param.value());
            }
        }
        printStream.printf("Type built: %010d\nFields: %03d\nParams: %03d\n", type.getAnnotation(VirtualType.class).created(), fields.size(), params.size());
        printStream.println("Fields:");
        for(String field : fields) {
            printStream.println("\t" + field);
        }
        printStream.println("Params:");
        for(String param : params) {
            printStream.println("\t" + param);
        }
    }
}
