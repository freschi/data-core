package at.femoweb.data.core.internal.factories;

import at.femoweb.data.core.ConnectionFactory;
import at.femoweb.data.core.DatabaseDialect;
import at.femoweb.data.core.internal.dialects.MysqlDialect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by felix on 3/2/15.
 */
public class MysqlFactory implements ConnectionFactory {

    private ArrayList<Connection> connections;
    private String url;
    private String user;
    private String pass;

    @Override
    public void openConnection(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.pass = password;
        connections = new ArrayList<>();
    }

    @Override
    public boolean isSql() {
        return true;
    }

    @Override
    public boolean isJDBC() {
        return true;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return getConnection(false);
    }

    @Override
    public Connection getConnection(boolean newConnection) throws SQLException {
        if(newConnection || connections.size() == 0) {
            Connection connection = DriverManager.getConnection(url, user, pass);
            connections.add(connection);
            return connection;
        } else {
            return connections.get((int)(Math.random() * connections.size()));
        }
    }

    @Override
    public void closeAll() {
        for(Connection connection : connections) {
            try {
                if(!connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void destroy() {
        connections = null;
    }

    @Override
    public DatabaseDialect getDatabaseDialect() {
        return new MysqlDialect();
    }
}
