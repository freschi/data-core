package at.femoweb.data.core.internal.query;

import at.femoweb.data.core.Conditions;
import at.femoweb.data.core.ConnectionFactory;
import at.femoweb.data.core.Query;
import at.femoweb.data.core.SpecializedQuery;

import java.util.List;

/**
 * Created by felix on 3/2/15.
 */
public class BasicQuery<T> implements Query<T> {

    private Class<T> type;
    private ConnectionFactory factory;

    public BasicQuery(Class<T> type, ConnectionFactory factory) {
        this.type = type;
        this.factory = factory;
    }


    @Override
    public Query<T> select() {
        return new SelectQuery<T>(type, factory);
    }

    @Override
    public Query<T> delete() {
        //return new DeleteQuery<T>();
        return null;
    }

    @Override
    public Query<T> update() {
        //return new UpdateQuery<T>();
        return null;
    }

    @Override
    public Query<T> insert() {
        //return new InsertQuery<T>();
        return null;
    }

    @Override
    public SpecializedQuery field(Class<?> entity, String field) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> where(Class<?> entity, String field, Object value) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> where(Conditions conditions) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> groupBy(String field) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> orderBy(String field, Order order) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> orderBy(String field) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> having(String field, Object value) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> having(Conditions conditions) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> set(String field, Object value) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> set(String field, Object[] values) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Query<T> values(T[] objects) {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public List<T> execute() {
        throw new UnsupportedOperationException("Invalid query type. You need to specify a query type before");
    }

    @Override
    public Class<T> type() {
        return type;
    }
}
