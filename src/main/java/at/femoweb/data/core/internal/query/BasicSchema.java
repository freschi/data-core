package at.femoweb.data.core.internal.query;

import at.femoweb.data.core.ConnectionFactory;
import at.femoweb.data.core.Query;
import at.femoweb.data.core.Schema;
import at.femoweb.data.core.Table;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by felix on 1/26/15.
 */
public class BasicSchema implements Schema {

    private List<Table> tables;
    private String name;
    private ConnectionFactory factory;

    public BasicSchema(String name, ConnectionFactory factory) {
        this.factory = factory;
        tables = new ArrayList<>();
        this.name = name;
    }

    public void addTable(Table table) {
        this.tables.add(table);
    }


    @Override
    public Table getTable(int index) {
        return tables.get(index);
    }

    @Override
    public Table getTable(String name) {
        return null;
    }

    @Override
    public Table getTable(Class<?> identifier) {
        return null;
    }

    @Override
    public void printInfo(PrintStream out) {
        out.println("Schema Name: " + name);
        out.println("Entities:");
        for(Table table : tables) {
            out.println("\t" + table.getName());
        }
    }

    @Override
    public <T> Query<T> query(Class<T> type) {
        return new BasicQuery<>(type, factory);
    }

    @Override
    public int getTableCount() {
        return tables.size();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
