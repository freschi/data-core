package at.femoweb.data.core.internal.query;

import at.femoweb.data.core.Table;

/**
 * Created by felix on 1/26/15.
 */
public class BasicTable implements Table {

    private String name;


    public BasicTable(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
