package at.femoweb.data.core.internal.query;

import java.lang.reflect.Field;

/**
 * Created by felix on 3/2/15.
 */
public abstract class DataObject {

    public boolean hasField(String name)  {
        try {
            getClass().getDeclaredField(name);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    public <T> T getField(Class<T> type, String name) {
        try {
            Field field = getClass().getDeclaredField(name);
            field.setAccessible(true);
            if(type.isAssignableFrom(field.getType())) {
                return (T) field.get(this);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTypeName() {
        return getClass().getName();
    }

    public <T> void setField(String name, T value) {
        try {
            Field field = getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(this, value);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        String val = getClass().getSimpleName();
        val += "{";
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field field = getClass().getDeclaredFields()[i];
            field.setAccessible(true);
            val += field.getName() + "=";
            try {
                val += "'" + field.get(this).toString() + "'";
            } catch (IllegalAccessException e) {
                val += "'null'";
                e.printStackTrace();
            }
            if(i != getClass().getDeclaredFields().length - 1) {
                val += ", ";
            }
            field.setAccessible(false);
        }
        val += "}";
        return val;
    }
}
