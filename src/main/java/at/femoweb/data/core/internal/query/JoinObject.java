package at.femoweb.data.core.internal.query;

import at.femoweb.data.core.annotations.Entity;

/**
 * Created by felix on 3/3/15.
 */
public class JoinObject {

    private Class<?> srcType;
    private String srcField;
    private Class<?> dstType;
    private String dstField;

    public JoinObject(Class<?> srcType, String srcField, Class<?> dstType, String dstField) {
        this.srcType = srcType;
        this.srcField = srcField;
        this.dstType = dstType;
        this.dstField = dstField;
    }

    public String renderJoin() {
        return " INNER JOIN " + dstType.getAnnotation(Entity.class).tableName() + " ON " + srcType.getAnnotation(Entity.class).tableName() +
                "." + srcField + " = " + dstType.getAnnotation(Entity.class).tableName() + "." + dstField;
    }
}
