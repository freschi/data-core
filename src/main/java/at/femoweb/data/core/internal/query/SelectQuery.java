package at.femoweb.data.core.internal.query;

import at.femoweb.data.core.*;
import at.femoweb.data.core.annotations.Convert;
import at.femoweb.data.core.annotations.Entity;
import javassist.CannotCompileException;
import javassist.NotFoundException;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;

/**
 * Created by felix on 3/2/15.
 */
public class SelectQuery<T> implements Query<T> {

    private Class<T> type;
    private ConnectionFactory factory;
    private ArrayList<Condition> conditions;

    public SelectQuery(Class<T> type, ConnectionFactory factory) {
        this.type = type;
        this.factory = factory;
        this.conditions = new ArrayList<>();
    }

    @Override
    public Query<T> select() {
        throw new UnsupportedOperationException("You can specify an operation only once!");
    }

    @Override
    public Query<T> delete() {
        throw new UnsupportedOperationException("You can specify an operation only once!");
    }

    @Override
    public Query<T> update() {
        throw new UnsupportedOperationException("You can specify an operation only once!");
    }

    @Override
    public Query<T> insert() {
        throw new UnsupportedOperationException("You can specify an operation only once!");
    }

    @Override
    public SpecializedQuery field(Class<?> entity, String field) {
        try {
            SpecializedQuery query = new SpecializedSelectQuery(type, factory);
            query.field(entity, field);
            return query;
        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (CannotCompileException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Query<T> where(Class<?> entity, String field, Object value) {
        if(value instanceof Byte || value instanceof Character || value instanceof Short ||
                value instanceof Integer || value instanceof Long || value instanceof Float || value instanceof Double) {
            conditions.add(new WhereCondition(entity.getAnnotation(Entity.class).tableName(), field, Double.valueOf(value.toString())));
        } else {
            conditions.add(new WhereCondition(entity.getAnnotation(Entity.class).tableName(), field, String.valueOf(value)));
        }
        return this;
    }

    @Override
    public Query<T> where(Conditions conditions) {
        return null;
    }

    @Override
    public Query<T> groupBy(String field) {
        return null;
    }

    @Override
    public Query<T> orderBy(String field, Order order) {
        return null;
    }

    @Override
    public Query<T> orderBy(String field) {
        return null;
    }

    @Override
    public Query<T> having(String field, Object value) {
        return null;
    }

    @Override
    public Query<T> having(Conditions conditions) {
        return null;
    }

    @Override
    public Query<T> set(String field, Object value) {
        throw new UnsupportedOperationException("This operation is not possible with a SELECT query");
    }

    @Override
    public Query<T> set(String field, Object[] values) {
        throw new UnsupportedOperationException("This operation is not possible with a SELECT query");
    }

    @Override
    public Query<T> values(T[] objects) {
        throw new UnsupportedOperationException("This operation is not possible with a SELECT query");
    }

    @Override
    public List<T> execute() {
        return executeSelect();
    }

    private List<T> executeSelect() {
        String query = "SELECT ";
        String table = "";
        if(type.isAnnotationPresent(Entity.class))
            table = type.getAnnotation(Entity.class).tableName();
        Class<? extends T> type = type();
        for (int i = 0; i < type.getDeclaredFields().length; i++) {
            Field field = type.getDeclaredFields()[i];
            if(field.isAnnotationPresent(at.femoweb.data.core.annotations.Field.class)) {
                at.femoweb.data.core.annotations.Field f = field.getAnnotation(at.femoweb.data.core.annotations.Field.class);
                if(f.table().equals("")) {
                    query += table + "." + f.name();
                } else {
                    query += f.table() + "." + f.name();
                }
                //TODO tidy up comma setting algorithm
                if(i != type.getDeclaredFields().length - 1) {
                    query += ", ";
                }
            }
        }
        query += " FROM " + generateTableSet();
        if(conditions.size() > 0) {
            query += " WHERE ";
            for (int i = 0; i < conditions.size(); i++) {
                query += conditions.get(i).getTable() + "." + conditions.get(i).getField() + " = " + conditions.get(i).getValue();
            }
        }
        System.out.println(query);
        if(factory != null) {
            try {
                Connection connection = factory.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                ArrayList<T> ts = new ArrayList<>();
                if(resultSet.first()) {
                    do {
                        T t = type().newInstance();
                        for (int i = 0; i < type.getDeclaredFields().length; i++) {
                            Field field = type.getDeclaredFields()[i];
                            if (field.isAnnotationPresent(at.femoweb.data.core.annotations.Field.class)) {
                                at.femoweb.data.core.annotations.Field f = field.getAnnotation(at.femoweb.data.core.annotations.Field.class);
                                String name;
                                if (f.table().equals("")) {
                                    name = table + "." + f.name();
                                } else {
                                    name = f.table() + "." + f.name();
                                }
                                field.setAccessible(true);
                                if(field.isAnnotationPresent(Convert.class)) {
                                    Convert convert = field.getAnnotation(Convert.class);
                                    if(convert.databaseClass() == String.class) {
                                        ValueConverter converter = convert.converter().newInstance();
                                        field.set(t, converter.fromDb(resultSet.getString(name)));
                                    }
                                } else {
                                    if (field.getType() == String.class) {
                                        field.set(t, resultSet.getString(name));
                                    } else if (field.getType() == int.class) {
                                        field.set(t, resultSet.getInt(name));
                                    } else if (field.getType() == float.class) {
                                        field.set(t, resultSet.getFloat(name));
                                    }
                                }
                                field.setAccessible(false);
                            }
                        }
                        ts.add(t);
                    } while (resultSet.next());
                }
                return ts;
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return new ArrayList<>();
    }

    protected String generateTableSet() {
        return type().getAnnotation(Entity.class).tableName();
    }

    @Override
    public Class<T> type() {
        return type;
    }
}
