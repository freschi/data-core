package at.femoweb.data.core.internal.query;

import at.femoweb.data.core.ConnectionFactory;
import at.femoweb.data.core.SpecializedQuery;
import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.VirtualType;
import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by felix on 3/2/15.
 */
public class SpecializedSelectQuery extends SelectQuery<DataObject> implements SpecializedQuery {

    private ClassPool pool;
    private CtClass type;
    private static int counter = 0;
    private ConnectionFactory factory;
    private ArrayList<String> tables;
    private ArrayList<JoinObject> joins;
    private Class<?> baseType;
    private Class<DataObject> compileType;
    private ArrayList<String> fields;

    public SpecializedSelectQuery(Class<?> baseType, ConnectionFactory factory) throws NotFoundException, CannotCompileException {
        super(DataObject.class, factory);
        this.factory = factory;
        this.tables = new ArrayList<>();
        this.joins = new ArrayList<>();
        this.baseType = baseType;
        this.fields = new ArrayList<>();
        pool = ClassPool.getDefault();
        type = pool.makeClass("VirtualType_" + (counter++));
        type.setSuperclass(resolveClass(DataObject.class));
        Annotation annotation = new Annotation(type.getClassFile().getConstPool(), resolveClass(VirtualType.class));
        annotation.addMemberValue("created", new LongMemberValue(System.currentTimeMillis(), type.getClassFile().getConstPool()));
        AnnotationsAttribute attribute = new AnnotationsAttribute(type.getClassFile().getConstPool(), AnnotationsAttribute.visibleTag);
        attribute.addAnnotation(annotation);
        type.getClassFile().addAttribute(attribute);
    }

    private void addField(Class<?> entity, String field) throws NotFoundException, CannotCompileException {
        if(entity.isAnnotationPresent(Entity.class)) {
            if(!tables.contains(entity.getAnnotation(Entity.class).tableName())) {
                tables.add(entity.getAnnotation(Entity.class).tableName());
            }
            java.lang.reflect.Field f = null;
            for(java.lang.reflect.Field f_ : entity.getDeclaredFields()) {
                if(f_.isAnnotationPresent(Field.class) && f_.getAnnotation(Field.class).name().equals(field)) {
                    f = f_;
                    break;
                }
            }
            if(f == null) {
                return;
            }
            if(fields.contains(field)) {
                int count = 0;
                while (fields.contains(field + "_" + count)) count++;
                field = field + "_" + count;
            }
            fields.add(field);
            CtField ctField = new CtField(resolveClass(f.getType()), field, type);
            ctField.setModifiers(Modifier.PRIVATE);
            Annotation annotation = new Annotation(type.getClassFile().getConstPool(), resolveClass(Field.class));
            MemberValue memberValue = new StringMemberValue(f.getAnnotation(Field.class).name(), type.getClassFile().getConstPool());
            annotation.addMemberValue("name", memberValue);
            memberValue = new StringMemberValue(entity.getAnnotation(Entity.class).tableName(), type.getClassFile().getConstPool());
            annotation.addMemberValue("table", memberValue);
            AnnotationsAttribute attribute = new AnnotationsAttribute(type.getClassFile().getConstPool(), AnnotationsAttribute.visibleTag);
            attribute.addAnnotation(annotation);
            ctField.getFieldInfo().addAttribute(attribute);
            type.addField(ctField);
        }
    }

    private CtClass resolveClass(Class<?> type) throws NotFoundException {
        return pool.get(type.getName());
    }

    @Override
    public SpecializedQuery field(Class<?> entity, String field) {
        try {
            addField(entity, field);
        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (CannotCompileException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public Class<DataObject> type() {
        if(compileType == null) {
            try {
                type.writeFile("test");
                return compileType = type.toClass();
            } catch (CannotCompileException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return DataObject.class;
        } else {
            return compileType;
        }
    }


   /* @Override
    public List<DataObject> execute() {
        ArrayList list = new ArrayList();
        try {
            list.add(type().newInstance());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return list;
    }*/


    @Override
    protected String generateTableSet() {
        String tableSet = " " + baseType.getAnnotation(Entity.class).tableName();
        for(JoinObject join : joins) {
            tableSet += join.renderJoin();
        }
        return tableSet;
    }

    @Override
    public SpecializedQuery on(Class<?> srcTable, String srcField, Class<?> dstTable, String dstField) {
        joins.add(new JoinObject(srcTable, srcField, dstTable, dstField));
        return this;
    }

    @Override
    public SpecializedQuery param(String field) {
        return null;
    }
}
