package at.femoweb.data.core.internal.query;

import at.femoweb.data.core.Condition;

/**
 * Created by felix on 3/25/15.
 */
public class WhereCondition implements Condition {

    private String table;
    private String field;
    private String value;

    public WhereCondition(String table, String field, double value) {
        this.table = table;
        this.field = field;
        this.value = String.valueOf(value);
    }

    public WhereCondition(String table, String field, String value) {
        this.table = table;
        this.field = field;
        this.value = "'" + value + "'";
    }

    @Override
    public String getTable() {
        return table;
    }

    @Override
    public String getField() {
        return field;
    }

    @Override
    public String getValue() {
        return value;
    }
}
