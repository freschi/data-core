package at.femoweb.data.core.test;

import at.femoweb.data.core.Condition;
import at.femoweb.data.core.DataContext;
import at.femoweb.data.core.Schema;
import at.femoweb.data.core.debug.VirtualTypePrinter;
import at.femoweb.data.core.internal.query.DataObject;
import at.femoweb.data.core.test.world.City;
import at.femoweb.data.core.test.world.Country;
import at.femoweb.data.core.test.world.CountryLanguage;
import at.femoweb.data.core.test.world.WorldSchema;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by felix on 1/26/15.
 */

public class EntityTest {

    @Test
    public void testGenerateSchema() throws Exception {
        Schema schema = DataContext.generateSchema(TestSchema.class, null);

        assertEquals("Number of Tables", 2, schema.getTableCount());
        assertEquals("Table Name", "t_test", schema.getTable(0).getName());
        schema.printInfo(System.out);
    }

    @Test
    public void testQuery() throws Exception {
        Schema schema = DataContext.generateSchema(TestEntity.class, null);
        List<TestEntity> test = schema.query(TestSchema.TABLE_TEST).select().where(TestSchema.TABLE_TEST, "t_id", 1).execute();
        assertEquals(0, test.size());
    }

    @Test
    public void testVirtualTypes() throws Exception {
        Schema schema = DataContext.generateSchema(TestEntity.class, null);
        Class<? extends DataObject> type = (Class<? extends DataObject>) schema.query(TestSchema.TABLE_TEST).select().field(TestSchema.TABLE_TEST, TestEntity.COLUMN_ID)
                .field(TestSchema.TABLE_TEST, TestEntity.COLUMN_NAME).field(TestSchema.TABLE_TEST_2, TestEntity2.COLUMN_NAME).type();
        VirtualTypePrinter.printVirtualType(type);
        List<? extends DataObject> objects = (List<? extends DataObject>) schema.query(TestSchema.TABLE_TEST).select().field(TestSchema.TABLE_TEST, TestEntity.COLUMN_ID).execute();
        assertEquals(true, DataObject.class.isAssignableFrom(type));
    }

    @Test
    public void testSelect() throws Exception {
        Schema schema = DataContext.generateSchema(WorldSchema.class, DataContext.connect("jdbc:mysql://localhost:3306/world", "root", ""));
        schema.printInfo(System.out);
        List<City> cities = schema.query(City.class).select().execute();
        for(City city : cities) {
            System.out.println(city);
        }
        List<CountryLanguage> languages = schema.query(CountryLanguage.class).select().execute();
        for(CountryLanguage language : languages) {
            System.out.println(language);
        }
        List<Country> countries = schema.query(Country.class).select().execute();
        for(Country country : countries) {
            System.out.println(country);
        }
        List<DataObject> countriesWithCapitals = schema.query(Country.class).select().field(Country.class, "Name").field(Country.class, "SurfaceArea")
                .field(City.class, "Name").on(Country.class, "Capital", City.class, "ID").field(City.class, "Population").execute();
        for(DataObject countryWithCapital : countriesWithCapitals) {
            System.out.println(countryWithCapital);
        }
    }
}
