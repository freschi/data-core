package at.femoweb.data.core.test;

import at.femoweb.data.core.Constraint;

import java.util.List;

/**
 * Created by felix on 1/26/15.
 */
public class TestConstraint implements Constraint<TestEntity> {

    @Override
    public boolean validOperationInsert(List<TestEntity> elements, final TestEntity insert) {
         return elements.stream().filter(entity -> entity.getName().equalsIgnoreCase(insert.getName())).count() <= 1;
    }

    @Override
    public boolean validOperationUpdate(List<TestEntity> elements, TestEntity update) {
        return elements.stream().filter(entity -> entity.getName().equalsIgnoreCase(update.getName())).count() <= 1;
    }

    @Override
    public boolean validOperationDelete(List<TestEntity> elements, TestEntity delete) {
        return true;
    }
}
