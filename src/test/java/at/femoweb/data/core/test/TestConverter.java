package at.femoweb.data.core.test;

import at.femoweb.config.ConfigReader;
import at.femoweb.config.entries.Group;
import at.femoweb.data.core.ValueConverter;

import java.io.UnsupportedEncodingException;

/**
 * Created by felix on 1/26/15.
 */
public class TestConverter implements ValueConverter<String, Group> {

    @Override
    public Group fromDb(String value) {
        Group group = new Group();
        ConfigReader.readGroup(value, group);
        return group;
    }

    @Override
    public String toDb(Group value) {
        return value.export();
    }
}
