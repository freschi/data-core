package at.femoweb.data.core.test;

import at.femoweb.config.entries.Group;
import at.femoweb.data.core.annotations.*;

/**
 * Created by felix on 1/26/15.
 */
@Entity(creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE, schema = TestSchema.class, tableName = "t_test")
@Constraint(constraints = TestConstraint.class)
public class TestEntity {

    public static final String COLUMN_ID = "t_id";
    public static final String COLUMN_NAME = "t_name";
    public static final String COLUMN_CONF = "t_conf";

    @Field(name = COLUMN_ID)
    @AutoIncrement
    @Id
    private int id;

    @Field(name = COLUMN_NAME)
    public String name;

    @Field(name = COLUMN_CONF)
    @Convert(converter = TestConverter.class)
    public Group conf;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group getConf() {
        return conf;
    }

    public void setConf(Group conf) {
        this.conf = conf;
    }
}
