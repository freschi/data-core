package at.femoweb.data.core.test;

import at.femoweb.data.core.annotations.AutoIncrement;
import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.Id;

/**
 * Created by felix on 3/2/15.
 */
@Entity(creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE, schema = TestSchema.class, tableName = "t2_test")
public class TestEntity2 {

    public static final String COLUMN_ID = "t2_id";
    public static final String COLUMN_NAME = "t2_name";

    @Field(name = COLUMN_ID)
    @Id
    @AutoIncrement
    private int id;

    @Field(name = COLUMN_NAME)
    private String name;

}
