package at.femoweb.data.core.test;

import at.femoweb.data.core.annotations.Schema;
import at.femoweb.data.core.annotations.Table;
import at.femoweb.data.core.annotations.Tables;

/**
 * Created by felix on 1/26/15.
 */
@Schema(name = "test")
@Tables({
        @Table(TestEntity.class),
        @Table(TestEntity2.class)
})
public class TestSchema {

    public static final Class<TestEntity> TABLE_TEST = TestEntity.class;
    public static final Class<TestEntity2> TABLE_TEST_2 = TestEntity2.class;
}
