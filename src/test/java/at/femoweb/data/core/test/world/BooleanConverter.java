package at.femoweb.data.core.test.world;

import at.femoweb.data.core.ValueConverter;

/**
 * Created by felix on 3/2/15.
 */
public class BooleanConverter implements ValueConverter<String, Boolean> {

    @Override
    public Boolean fromDb(String value) {
        return value.equals("T");
    }

    @Override
    public String toDb(Boolean value) {
        return value ? "T": "F";
    }
}
