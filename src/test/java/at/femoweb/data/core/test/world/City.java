package at.femoweb.data.core.test.world;

import at.femoweb.data.core.annotations.AutoIncrement;
import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.Id;

/**
 * Created by felix on 3/2/15.
 */
@Entity(tableName = "City", schema = WorldSchema.class, creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE)
public class City {

    @Field(name = "ID")
    @Id
    @AutoIncrement
    private int id;

    @Field(name = "Name")
    private String name;

    @Field(name = "CountryCode")
    private String countryCode;

    @Field(name = "District")
    private String district;

    @Field(name = "Population")
    private int population;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", district='" + district + '\'' +
                ", population=" + population +
                '}';
    }
}
