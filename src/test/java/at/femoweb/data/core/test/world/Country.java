package at.femoweb.data.core.test.world;

import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.Id;

/**
 * Created by felix on 3/3/15.
 */
@Entity(schema = WorldSchema.class, tableName = "Country", creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE)
public class Country {

    @Field(name = "Code")
    @Id
    private String code;

    @Field(name = "Name")
    private String name;

    @Field(name = "Continent")
    private String continent;

    @Field(name = "Region")
    private String region;

    @Field(name = "SurfaceArea")
    private float surfaceArea;

    @Field(name = "IndepYear")
    private int indepYear;

    @Field(name = "Population")
    private int population;

    @Field(name = "LifeExpectancy")
    private float lifeExpectancy;

    @Field(name = "GNP")
    private float gnp;

    @Field(name = "GNPOld")
    private float gnpOld;

    @Field(name = "LocalName")
    private String localName;

    @Field(name = "GovernmentForm")
    private String governmentForm;

    @Field(name = "HeadOfState")
    private String headOfState;

    @Field(name = "Capital")
    private int capital;

    @Field(name = "Code2")
    private String code2;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public float getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(float surfaceArea) {
        this.surfaceArea = surfaceArea;
    }

    public int getIndepYear() {
        return indepYear;
    }

    public void setIndepYear(int indepYear) {
        this.indepYear = indepYear;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public float getLifeExpectancy() {
        return lifeExpectancy;
    }

    public void setLifeExpectancy(float lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }

    public float getGnp() {
        return gnp;
    }

    public void setGnp(float gnp) {
        this.gnp = gnp;
    }

    public float getGnpOld() {
        return gnpOld;
    }

    public void setGnpOld(float gnpOld) {
        this.gnpOld = gnpOld;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getGovernmentForm() {
        return governmentForm;
    }

    public void setGovernmentForm(String governmentForm) {
        this.governmentForm = governmentForm;
    }

    public String getHeadOfState() {
        return headOfState;
    }

    public void setHeadOfState(String headOfState) {
        this.headOfState = headOfState;
    }

    public int getCapital() {
        return capital;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    @Override
    public String toString() {
        return "Country{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", continent='" + continent + '\'' +
                ", region='" + region + '\'' +
                ", surfaceArea=" + surfaceArea +
                ", indepYear=" + indepYear +
                ", population=" + population +
                ", lifeExpectancy=" + lifeExpectancy +
                ", gnp=" + gnp +
                ", gnpOld=" + gnpOld +
                ", localName='" + localName + '\'' +
                ", governmentForm='" + governmentForm + '\'' +
                ", headOfState='" + headOfState + '\'' +
                ", capital=" + capital +
                ", code2='" + code2 + '\'' +
                '}';
    }
}
