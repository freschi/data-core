package at.femoweb.data.core.test.world;

import at.femoweb.data.core.annotations.Convert;
import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.Id;

/**
 * Created by felix on 3/2/15.
 */
@Entity(creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE, tableName = "CountryLanguage", schema = WorldSchema.class)
public class CountryLanguage {

    @Field(name = "CountryCode")
    @Id
    private String countryCode;

    @Field(name = "Language")
    @Id
    private String language;

    @Field(name = "IsOfficial")
    @Convert(converter = BooleanConverter.class)
    private boolean isOfficial;

    @Field(name = "Percentage")
    private float percentage;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isOfficial() {
        return isOfficial;
    }

    public void setOfficial(boolean isOfficial) {
        this.isOfficial = isOfficial;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    @Override
    public String toString() {
        return "CountryLanguage{" +
                "countryCode='" + countryCode + '\'' +
                ", language='" + language + '\'' +
                ", isOfficial=" + isOfficial +
                ", percentage=" + percentage +
                '}';
    }
}
