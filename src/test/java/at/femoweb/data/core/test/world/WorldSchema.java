package at.femoweb.data.core.test.world;

import at.femoweb.data.core.annotations.Schema;
import at.femoweb.data.core.annotations.Table;
import at.femoweb.data.core.annotations.Tables;

/**
 * Created by felix on 3/2/15.
 */
@Schema(name = "world")
@Tables(
        {
                @Table(City.class),
                @Table(CountryLanguage.class)
        }
)
public class WorldSchema {
}
